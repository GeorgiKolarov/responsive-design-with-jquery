$(document).ready(function () {
    $('.title').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active').siblings('.cont').slideToggle();
        $(this).parent().siblings().find('.title').removeClass('active').siblings('.cont').slideUp();

        if ($('.title').hasClass('active')) {
            $(this).html('Click to close <span class="arrow open"></span>');
        } else {
            $(this).html('Click to open <span class="arrow close"></span>');
        }

    });
    $('.flags-menu li a img').on('click', function () {
        $('.flags-menu li a img.active-flag').removeClass('active-flag');
        $(this).addClass('active-flag');
    });
    
$("#menu-button").click(function(){
    $(this).toggleClass("active");
    $("#line-1").toggleClass("active");
    $("#line-2").toggleClass("active");
    $("#line-3").toggleClass("active");
    $(".main-menu").slideToggle("slow");
  });
});